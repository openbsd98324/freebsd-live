Disk /dev/sdb: 465.7 GiB, 500074283008 bytes, 976707584 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x00000000

Device     Boot   Start      End Sectors  Size Id Type
/dev/sdb1  *         64  3686514 3686451  1.8G a9 NetBSD
/dev/sdb2       3688448  8292351 4603904  2.2G a5 FreeBSD
/dev/sdb3       8292352 12896255 4603904  2.2G 83 Linux
