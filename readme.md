

v4: preinstalled i386 13 for newfs da1s1, copy, and run from grub with kfreebsd. 
    fstab has da0s1a to run from memstick 


v5: FreeBSD Live, preinstalled with grub:
 
freebsd live (i386), duo with netbsd live (amd64) running on memstick. (1.6gb, url: https://gitlab.com/openbsd98324/freebsd-live/-/raw/main/v5-freebsd-live-v1/freebsd-live-1651931173-p12-13-i386-netfree-grub86-lite-v1.img.gz )

netbsd has grub2 (with X11/Xorg, ctwm)
and freebsd live is on second partition.

For INTEL, type at boot loader [3] and "vbe set 0x17e"  boot

DISK MOUNT:  ufs:/dev/da0s2a 
(adapt accordingly.)

Have Fun!


